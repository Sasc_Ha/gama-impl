/***
* Name: CatchShyPokemon
* Author: Sascha
* Description: 
* 
* Extending: fieldofvision from Patrick Taillandier Tags: perception, spatial_computation, masked_by
*
***/

model PokemonShyness

global {

	int nb_pokemon <- 4 parameter: true;
	//precision used for the masked_by operator (default value: 120): the higher the most accurate the perception will be, but it will require more computation
	int precision <- 200 parameter: true;
	int nb_obstacles <- 14 parameter: true;
	float perception_distance <- 40.0;

	
	
	geometry walkable_environment <- copy(shape);

	init {
		create obstacle number: nb_obstacles {
			shape <- rectangle(3 + rnd(15), 1 + rnd(10));
			walkable_environment <- walkable_environment - shape;
		}

		create pokemon number: nb_pokemon{
			location <- any_location_in(walkable_environment);
		}

		create pedestrian {
			location <- any_location_in(walkable_environment);
		}

	}

}

species obstacle {

	aspect default {
		draw shape color: #gray border: #black;
	}

}
/**
 * 
 * pedestrian simulats an scary entity which moves around and pursue pokemons if they showed up once in pedestrians perceived_are
 * 
 * 	- is faster than pokemon
 * 	- has lower perceived_area
 * 	- (is able to catch pokemon)
 */
species pedestrian skills: [moving] {
	geometry perceived_area;
	point target;
	

	reflex move {
		speed <- 3.3 ;
		if (target = nil) {
			if (perceived_area = nil) or (perceived_area.area < 2.0) {
				do wander bounds: walkable_environment;
			} else {
				target <- any_location_in(perceived_area);
			}
		} else {
			do goto target: target;
			if (location = target) {
				target <- nil;
			}
		}
		ask pokemon {
			if(covers(myself.perceived_area, self.location)){
				myself.target <- self.location;
			}else if(distance_to(myself.location, self.location)<2){
				ask self{
					do die;
				}
			}
		}
	}
	/**
	 * distribes above moving implementation
	 * 
	 */
	
	aspect default {
		draw circle(1) color: #red;
	}
	reflex update_perception {
		perceived_area <- (cone(heading - 30, heading + 30) intersection world.shape) intersection circle(20.00);
		if (perceived_area != nil) {
			perceived_area <- perceived_area masked_by (obstacle, precision);
		}
	}
	aspect perception {
		if (perceived_area != nil) {
			draw perceived_area color: #black;
			draw circle(0.2) at: target color: #red;
		}
	}
}
/**
 * 
 * pokemon_actor is shy wants to escape from pedestrian
 * 
 * 	- avoids colliding with obstacles
 * 	- is slower than pedestrian
 * 	- has an additional perceived_area 
 * 
 * 
 */
species pokemon skills: [moving] {

	geometry perceived_area;
	geometry escape_area;
	point target;

	reflex move {
		speed <- 0.8;
		if (target = nil) {
			if (perceived_area = nil) or (perceived_area.area < 2.0) {
				do wander bounds: walkable_environment;
			} else {
				target <- any_location_in(perceived_area);
			}
		} else {
				do goto target: target on:walkable_environment;
			if (location = target) {
				target <- nil;
			}
		}
		ask pedestrian{
			if(covers(myself.perceived_area, self.location)){
				ask myself{
						//write self.name;
						self.heading <- 90.0+ rnd(270.0);
						escape_area <- (cone(heading - 11, heading + 11) intersection world.shape) intersection circle(22);
						if (escape_area != nil) {
							escape_area <- escape_area masked_by (obstacle, precision);
						}
						self.target <- any_location_in(self.escape_area);
				}
			}
		}
	}
	
	/**
	 * distribes above moving implementation
	 * 
	 */
	reflex update_escape {
		escape_area <- (cone(heading - 11, heading + 11) intersection world.shape) intersection circle(22);
		if (escape_area != nil) {
			escape_area <- escape_area masked_by (obstacle, precision);
		}
	}
	reflex update_perception {
		perceived_area <- (cone(heading - 60, heading + 60) intersection world.shape) intersection circle(perception_distance);
		if (perceived_area != nil) {
			perceived_area <- perceived_area masked_by (obstacle, precision);
		}
	}
	aspect default {
			draw triangle(2) rotate: 90 + heading color: #blue;
	}
	aspect perception {
		if (perceived_area != nil) {
			draw perceived_area color: #green;
			draw circle(0.2) at: target color: #red;
		}
	}
}
/**
 * experiment runs the runtime and the visualisation
 * 
 */

experiment fieldofvision type: gui {
	float minimum_cycle_duration <- 0.05;
	output {
		display view synchronized: true {
			species obstacle;
			species pokemon aspect: perception transparency: 0.5;
			species pokemon;
			species pedestrian;
			species pedestrian aspect: perception transparency: 0.5;
		}

	}

}